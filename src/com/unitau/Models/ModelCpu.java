package com.unitau.Models;

public class ModelCpu {

	private final int cores;
	private final double core_frequency;
	private final double temperature;
	private final String model_name;
	private final int cache_size;

	public ModelCpu(int cores, double core_frequecy, double temperature, String model_name, int cache_size) {
		this.cores = cores;
		this.core_frequency = core_frequecy;
		this.temperature = temperature;
		this.model_name = model_name;
		this.cache_size = cache_size;
	}

	public int getCores() {
		return cores;
	}

	public double getCore_frequency() {
		return core_frequency;
	}

	public double getTemperature() {
		return temperature;
	}

	public String getModel_name() {
		return model_name;
	}

	public int getCache_size() {
		return cache_size;
	}

	@Override
	public String toString() {
		return "ModelCpu [cores=" + cores + ", core_frequency=" + core_frequency + ", temperature=" + temperature + ", model_name=" + model_name + ", cache_size=" + cache_size + "]";
	}

}
