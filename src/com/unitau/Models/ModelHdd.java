package com.unitau.Models;

public class ModelHdd {
	private final double free;
	private final double total;
	private final double used;

	public ModelHdd(double free, double total, double used) {
		this.free = free;
		this.total = total;
		this.used = used;
	}

	public double getFree() {
		return free;
	}

	public double getTotal() {
		return total;
	}

	public double getUsed() {
		return used;
	}

	@Override
	public String toString() {
		return "ModelHdd [free=" + free + ", total=" + total + ", used=" + used + "]";
	}

}
