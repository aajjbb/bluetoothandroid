package com.unitau.Models;

public interface Parceable<T> {
	public T parce(String json);
}
