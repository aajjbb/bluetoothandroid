package com.unitau.Models;

public class ModelSystemInfo {
	private ModelSystem system;
	private ModelHdd hdd;
	private ModelRam ram;
	private ModelCpu cpu;
	private int BCC;

	public ModelSystemInfo(ModelSystem system, ModelHdd hdd, ModelRam ram, ModelCpu cpu) {
		this.system = system;
		this.hdd = hdd;
		this.ram = ram;
		this.cpu = cpu;
	}

	public ModelSystemInfo() {

	}

	public void setSystem(ModelSystem system) {
		this.system = system;
	}

	public void setHdd(ModelHdd hdd) {
		this.hdd = hdd;
	}

	public void setRam(ModelRam ram) {
		this.ram = ram;
	}

	public void setCpu(ModelCpu cpu) {
		this.cpu = cpu;
	}

	public void setBCC(int bCC) {
		BCC = bCC;
	}

	public ModelSystem getSystem() {
		return system;
	}

	public ModelHdd getHdd() {
		return hdd;
	}

	public ModelRam getRam() {
		return ram;
	}

	public ModelCpu getCpu() {
		return cpu;
	}

	public int getBCC() {
		return BCC;
	}

	@Override
	public String toString() {
		return "ModelSystemInfo [system=" + system + ", hdd=" + hdd + ", ram=" + ram + ", cpu=" + cpu + ", BCC=" + BCC + "]";
	}

}
