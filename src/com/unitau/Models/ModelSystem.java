package com.unitau.Models;

public class ModelSystem {

	private final String so;
	private final String user;
	private final String kernel_version;
	private final String architecture;
	private final String ip;

	public ModelSystem(String so, String user, String kernel, String architecture, String ip) {

		this.so = so;
		this.user = user;
		this.kernel_version = kernel;
		this.architecture = architecture;
		this.ip = ip;
	}

	public String getSo() {
		return so;
	}

	public String getUser() {
		return user;
	}

	public String getKernel_version() {
		return kernel_version;
	}

	public String getArchitecture() {
		return architecture;
	}

	public String getIp() {
		return ip;
	}

	@Override
	public String toString() {
		return "ModelSystem [so=" + so + ", user=" + user + ", kernel_version=" + kernel_version + ", architecture=" + architecture + ", ip=" + ip + "]";
	}

}
