package com.unitau.mrs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.Menu;

import com.example.mrs.R;
import com.unitau.Models.ModelSystemInfo;

public class MainActivity_AsyncTask extends Activity {

	private final String uuid = "38709c11-9262-42a3-a0b1-be2ae5d52a90";
	private final static int REQUEST_ENABLE_BT = 1;
	private List<BluetoothDevice> discoveredDevices;
	private BluetoothAdapter bluetoothAdapter;
	private AcceptThreads acceptThreads;
	private HandleTextViews handle_menu;
	private ConnectedThread connectedThread;
	private HandleMonitor handle_monitor;
	private DialogErrorControl[] errors;
	private final Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == 1) {
				connect();
				openDiscoverableToMyDevice(60 * 60);
				return true;
			} else if (msg.what == 2) {
				Log.i("MESSAGE WHAT", msg.what + "");
				setTitle(msg.obj.toString());
				return true;
			} else if (msg.what == 3) {// medium temp
				String[] tmp = (String[]) msg.obj;
				errors[0].show(tmp[0], tmp[1]);
				return true;
			} else if (msg.what == 4) {// high temp
				String[] tmp = (String[]) msg.obj;
				errors[1].show(tmp[0], tmp[1]);
				return true;
			} else if (msg.what == 5) { // low hd
				String[] tmp = (String[]) msg.obj;
				errors[2].show(tmp[0], tmp[1]);
				return true;
			} else {
				return false;
			}
		}
	});

	private final BroadcastReceiver deviceDiscover = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				discoveredDevices.add(device);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle("System monitor");
		discoveredDevices = new ArrayList<BluetoothDevice>();
		getAdapter();
		enableBluetooth();
		handle_menu = new HandleTextViews(this);
		handle_monitor = new HandleMonitor(this);
		errors = new DialogErrorControl[3];
		errors[0] = new DialogErrorControl(this);// medium temp
		errors[1] = new DialogErrorControl(this);// high temp
		errors[2] = new DialogErrorControl(this);// low hd
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (connectedThread != null) {
			connectedThread.cancel();
		}
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// unregisterReceiver(deviceDiscover);
	}

	private BluetoothAdapter getAdapter() {
		if (bluetoothAdapter == null)
			return bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		return bluetoothAdapter;
	}

	private boolean enableBluetooth() {
		if (bluetoothAdapter == null) {
			return false;
		} else {
			if (!bluetoothAdapter.isEnabled()) {
				Thread th = new Thread(new Runnable() {
					@Override
					public void run() {
						Intent enableDevIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						startActivityForResult(enableDevIntent, REQUEST_ENABLE_BT);
						Log.i("SOCKET", "WAITING");
						while (bluetoothAdapter.getState() != BluetoothAdapter.STATE_ON) {

						}
						Message msg = new Message();
						msg.what = 1;
						handler.sendMessage(msg);
					}
				});
				th.start();
				return true;
			} else {
				connect();
				openDiscoverableToMyDevice(60 * 60);
				return true;
			}
		}
	}

	private void openDiscoverableToMyDevice(int seconds) {
		if (bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, seconds);
			startActivity(discoverableIntent);
		}
	}

	private void getUnpairedDevices() {
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(deviceDiscover, filter);
		bluetoothAdapter.startDiscovery();
	}

	private Set<BluetoothDevice> getPairedDevices() {
		Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
		if (devices.size() > 0) {
			for (BluetoothDevice dev : devices) {
				if (dev != null) {
					Log.i("Dev Name", dev.getName());
					ParcelUuid[] p = dev.getUuids();
					if (p == null)
						break;
					for (ParcelUuid i : p) {
						if (i != null)
							Log.i("UUID", i.toString());
					}
				}
			}
			return devices;
		} else {
			Log.i("Error", "No paired device found.");
			return null;
		}
	}

	private void connect() {
		acceptThreads = new AcceptThreads();
		acceptThreads.execute();
	}

	private class AcceptThreads extends AsyncTask<Void, Void, Void> {
		private BluetoothServerSocket serverSocket;

		public AcceptThreads() {
			UUID MY_UUID = UUID.fromString(uuid);
			BluetoothServerSocket temporaryServerSocket = null;
			try {
				Log.i("Create the Server Socket", "aa");
				temporaryServerSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("SERVER_UUID", MY_UUID);
			} catch (IOException e) {
				Log.i("Error create the server socket", "ERR");
				e.printStackTrace();
			}
			serverSocket = temporaryServerSocket;
			Log.i("Server Socket ok", "aa");
		}

		@Override
		protected Void doInBackground(Void... params) {
			BluetoothSocket socket = null;
			while (true) {
				try {
					Log.i("Message:", "Waiting accept");
					if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
						Message msg = new Message();
						msg.what = 2;
						msg.obj = "System monitor - connecting";
						handler.sendMessage(msg);
						socket = serverSocket.accept();
					}
				} catch (IOException e) {
					Log.i("ERR", "Error on socket");
					break;
				}
				if (socket != null) {
					Log.i("Message:", "Socket accepted");
					connectedThread = new ConnectedThread(socket);
					connectedThread.execute();
					try {
						serverSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				}
			}
			return null;
		}

		public void cancel() {
			try {
				if (serverSocket != null)
					serverSocket.close();
			} catch (IOException e) {
			}
		}
	}

	private class ConnectedThread extends AsyncTask<Void, Void, Void> {
		private final BluetoothSocket deviceSocket;
		private final InputStream inputStream;
		private final OutputStream outputStream;

		public ConnectedThread(BluetoothSocket socket) {
			handle_menu.fillListsToHandleMenu();
			Log.i("Creating connected thread", "aa");
			deviceSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {

			}
			inputStream = tmpIn;
			outputStream = tmpOut;
		}

		@Override
		protected Void doInBackground(Void... params) {
			byte[] buffer = new byte[1024];
			int bytes;
			ModelSystemInfo infos;
			long timeTemp = 0;
			long timeHddFree = 0;
			Message msg = new Message();
			msg.what = 2;
			msg.obj = "System monitor - monitoring";
			handler.sendMessage(msg);
			while (true) {
				try {
					Log.i("Waiting for input", "w");
					bytes = inputStream.read(buffer);
					JSONParcer parser = new JSONParcer();
					String json = new String(buffer, 0, bytes);
					infos = parser.parce(json);
					if (infos.getBCC() != BCC(json.substring(0, json.indexOf("bcc_checksum") - 4) + "}}}")) {
						continue;
					}
					handle_menu.handleJsonToView(infos);
					msg = new Message();
					msg.obj = new double[] { infos.getCpu().getTemperature(), infos.getRam().getUsed() };
					msg.what = 1;
					handle_monitor.handlerGraphics.sendMessage(msg);
					double temperature = infos.getCpu().getTemperature();
					double pct_free = (infos.getHdd().getFree() * 100) / infos.getHdd().getTotal();
					if (temperature >= 36 && temperature <= 44) {
						if (!errors[1].isOpened() && !errors[0].isOpened()) {
							Message msga = new Message();
							msga.obj = new String[] { "Warning", "A temperatura do servidor esta elevada: " + String.format("%.1f", temperature) + "�C" };
							msga.what = 3;
							handler.sendMessage(msga);
						}
					} else if (temperature > 44) {
						if (!errors[1].isOpened()) {
							Message msga = new Message();
							msga.obj = new String[] { "Critical", "A temperatura do servidor esta muito elevada: " + String.format("%.1f", temperature) + "�C" };
							msga.what = 4;
							handler.sendMessage(msga);
						}
					}
					if (pct_free <= 10 + 1e-9) {
						if (!errors[1].isOpened() && !errors[2].isOpened()) {
							Message msga = new Message();
							msga.obj = new String[] { "Warning", "A quantidade livre de memoria da HD esta muito baixa: " + String.format("%.2f", pct_free) + "%" };
							msga.what = 5;
							handler.sendMessage(msga);
						}
					}
				} catch (IOException e) {
					Log.i("Error to read", "ERR");
					break;
				}
			}
			return null;
		}

		public void write(byte[] bytes) {
			try {
				outputStream.write(bytes);
			} catch (IOException e) {
			}
		}

		public void cancel() {
			try {
				if (deviceSocket.isConnected())
					deviceSocket.close();
			} catch (IOException e) {
			}
		}
	}

	public int BCC(String str) {
		return BCC(str, str.length());
	}

	/**
	 * Calcula recursivamente o BCC (Block check code) dos numeros para obter o
	 * checksum.
	 * 
	 * @param str
	 * @param n
	 * @return
	 */
	private int BCC(String str, int n) {
		int ret = 0;
		for (int i = 0; i < n; i++) {
			ret ^= str.charAt(i);
		}
		return ret;
	}
}
