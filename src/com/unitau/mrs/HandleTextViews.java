package com.unitau.mrs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.example.mrs.R;
import com.unitau.Models.ModelSystemInfo;

public class HandleTextViews {

	private final String SYSTEM_BAR = "system_bar";
	private final String HDD_BAR = "hdd_bar";
	private final String RAM_BAR = "ram_bar";
	private final String CPU_BAR = "cpu_bar";

	private final String SYSTEM_ITENS = "system_itens";
	private final String HDD_ITENS = "hdd_itens";
	private final String RAM_ITENS = "ram_itens";
	private final String CPU_ITENS = "cpu_itens";

	private Map<String, TextView> menubars;
	private Map<String, ArrayList<TextView>> subbars;
	private Activity father;

	public HandleTextViews(Activity f) {
		father = f;
		menubars = new HashMap<String, TextView>();
		subbars = new HashMap<String, ArrayList<TextView>>();
		fillListsToHandleMenu();
		addEvents();
		startHide();
	}

	public void startHide() {
		for (Map.Entry<String, ArrayList<TextView>> i : subbars.entrySet()) {
			if (i.getKey().equals(SYSTEM_ITENS) == false) {
				ArrayList<TextView> tmp = i.getValue();
				for (int j = 0; j < tmp.size(); j++) {
					tmp.get(j).setVisibility(View.GONE);
				}
			} else {
				ArrayList<TextView> tmp = i.getValue();
				for (int j = 0; j < tmp.size(); j++) {
					tmp.get(j).setVisibility(View.VISIBLE);
				}
			}
		}
	}

	public void fillListsToHandleMenu() {

		this.menubars.put(SYSTEM_BAR, getTv(R.id.tv_systembar));
		this.menubars.put(HDD_BAR, getTv(R.id.tv_hddbar));
		this.menubars.put(RAM_BAR, getTv(R.id.tv_rambar));
		this.menubars.put(CPU_BAR, getTv(R.id.tv_cpubar));

		ArrayList<ArrayList<TextView>> subbarss = new ArrayList<ArrayList<TextView>>();
		for (int i = 0; i < menubars.size(); i++) {
			subbarss.add(new ArrayList<TextView>());
		}
		subbarss.get(0).add(getTv(R.id.tv_operatingsystem));
		subbarss.get(0).add(getTv(R.id.tv_usersystem));
		subbarss.get(0).add(getTv(R.id.tv_kernelversionsystem));
		subbarss.get(0).add(getTv(R.id.tv_architecturesystem));
		subbarss.get(0).add(getTv(R.id.tv_ipsystem));

		subbarss.get(1).add(getTv(R.id.tv_freehdd));
		subbarss.get(1).add(getTv(R.id.tv_totalhdd));
		subbarss.get(1).add(getTv(R.id.tv_usedhdd));

		subbarss.get(2).add(getTv(R.id.tv_freeram));
		subbarss.get(2).add(getTv(R.id.tv_totalram));
		subbarss.get(2).add(getTv(R.id.tv_usedram));

		subbarss.get(3).add(getTv(R.id.tv_corescpu));
		subbarss.get(3).add(getTv(R.id.tv_core_frequencycpu));
		subbarss.get(3).add(getTv(R.id.tv_temperaurecpu));
		subbarss.get(3).add(getTv(R.id.tv_model_namecpu));
		subbarss.get(3).add(getTv(R.id.tv_cache_sizecpu));

		this.subbars.put(SYSTEM_ITENS, subbarss.get(0));
		this.subbars.put(HDD_ITENS, subbarss.get(1));
		this.subbars.put(RAM_ITENS, subbarss.get(2));
		this.subbars.put(CPU_ITENS, subbarss.get(3));

	}

	public void addEvents() {

		menubars.get(SYSTEM_BAR).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Map.Entry<String, ArrayList<TextView>> i : subbars.entrySet()) {
					if (i.getKey().equals(SYSTEM_ITENS) == false) {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.GONE);
						}
					} else {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.VISIBLE);
						}
					}
				}
			}
		});

		menubars.get(HDD_BAR).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Map.Entry<String, ArrayList<TextView>> i : subbars.entrySet()) {
					if (i.getKey().equals(HDD_ITENS) == false) {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.GONE);
						}
					} else {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.VISIBLE);
						}
					}
				}
			}
		});

		menubars.get(RAM_BAR).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Map.Entry<String, ArrayList<TextView>> i : subbars.entrySet()) {
					if (i.getKey().equals(RAM_ITENS) == false) {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.GONE);
						}
					} else {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.VISIBLE);
						}
					}
				}
			}
		});

		menubars.get(CPU_BAR).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Map.Entry<String, ArrayList<TextView>> i : subbars.entrySet()) {
					if (i.getKey().equals(CPU_ITENS) == false) {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.GONE);
						}
					} else {
						ArrayList<TextView> tmp = i.getValue();
						for (int j = 0; j < tmp.size(); j++) {
							tmp.get(j).setVisibility(View.VISIBLE);
						}
					}
				}
			}
		});
	}

	private TextView getTv(int id) {
		return (TextView) (father).findViewById(id);
	}

	public void handleJsonToView(final ModelSystemInfo model) {

		father.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// SO
				subbars.get(SYSTEM_ITENS).get(0).setText("SO: " + model.getSystem().getSo());

				// set USER
				subbars.get(SYSTEM_ITENS).get(1).setText("User: " + model.getSystem().getUser());

				// set KERNEL
				subbars.get(SYSTEM_ITENS).get(2).setText("Kernel Version: " + model.getSystem().getKernel_version());

				// set ARCHITECTURE
				subbars.get(SYSTEM_ITENS).get(3).setText("Architecture: " + model.getSystem().getArchitecture());

				// set IP
				subbars.get(SYSTEM_ITENS).get(4).setText("Ip Address: " + model.getSystem().getIp());

				// set HDDFREE
				subbars.get(HDD_ITENS).get(0).setText(String.format("Free: %.2fgb", model.getHdd().getFree()));

				// set HDDTOTAL
				subbars.get(HDD_ITENS).get(1).setText(String.format("Total: %.2fgb", model.getHdd().getTotal()));

				// set HDDUSED
				subbars.get(HDD_ITENS).get(2).setText(String.format("Used: %.2fgb", model.getHdd().getUsed()));

				// set RAMFREE
				subbars.get(RAM_ITENS).get(0).setText(String.format("Free: %.2fgb", model.getRam().getFree()));

				// set RAMTOTAL
				subbars.get(RAM_ITENS).get(1).setText(String.format("Total: %.2fgb", model.getRam().getTotal()));

				// set RAMUSED
				subbars.get(RAM_ITENS).get(2).setText(String.format("Used: %.2fgb", model.getRam().getUsed()));

				// set CORES_n
				subbars.get(CPU_ITENS).get(0).setText("Cores: " + Integer.toString(model.getCpu().getCores()));

				// set CORE_FREQUECY
				subbars.get(CPU_ITENS).get(1).setText(String.format("Core Frequency: %.2fGhz", model.getCpu().getCore_frequency()));

				
				// set TEMPERATURE
				subbars.get(CPU_ITENS).get(2).setText(String.format("Temperature: %.2f �C", model.getCpu().getTemperature()));

				// set PROCESSOR_MODEL
				subbars.get(CPU_ITENS).get(3).setText("Model Name: " + model.getCpu().getModel_name());

				// set PROCESSOR_MODEL
				subbars.get(CPU_ITENS).get(4).setText("Cache Size: " + Integer.toString(model.getCpu().getCache_size()));

			}
		});
	}
}
