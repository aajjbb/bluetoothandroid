package com.unitau.mrs;

import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class DialogErrorControl {
	private AlertDialog.Builder dialog;
	private Activity uiActivity;
	private boolean opened = false;
	private final short MINIMUMWAIT = 35; // in sec
	private long timeStart = 0;
	private boolean canShow = false;

	public DialogErrorControl(final Activity a) {
		uiActivity = a;
		uiActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				dialog = new AlertDialog.Builder(a);
				dialog.setCancelable(true);
				dialog.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						opened = false;
					}
				});
				dialog.setNegativeButton("Don't show again", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						opened = false;
						canShow = true;
					}
				});
			}
		});

	}

	public long getTimeNow() {
		Date curDate = new Date();
		long curMillis = curDate.getTime();
		return curMillis;
	}

	public void show(final String tittle, final String message) {
		if (!canShow) {
			long now = getTimeNow();
			if (now - timeStart > MINIMUMWAIT * 1000) {
				timeStart = now;
				uiActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						dialog.setMessage(message);
						dialog.setTitle(tittle);
						if (!opened) {
							opened = true;
							dialog.show();
						}
					}
				});
			}
		}
	}

	public boolean isOpened() {
		return opened;
	}
}
