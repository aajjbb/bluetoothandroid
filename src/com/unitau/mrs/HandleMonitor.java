package com.unitau.mrs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.example.mrs.R;

public class HandleMonitor {
	private XYPlot graphics;
	private List<Number> medicoesTemperature;
	private List<Number> medicoesRam;
	// private LineAndPointFormatter formatterTemperature;
	private LineAndPointFormatter formatterRam;

	public Handler handlerGraphics;
	private Activity father;

	public HandleMonitor(final Activity father) {
		Log.i("CHECKING", "RANGED");
		this.father = father;
		father.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				graphics = (XYPlot) father.findViewById(R.id.mygraphics);
				medicoesTemperature = new ArrayList<Number>();
				medicoesTemperature.add(0);
				graphics.setTitle("Graphic View - RAM");
				medicoesRam = new ArrayList<Number>();
				medicoesRam.add(0.0d);
				graphics.setTicksPerRangeLabel(1);
				graphics.setTicksPerDomainLabel(1);
				graphics.setDomainLabel("RECEIVED");
				graphics.setRangeLabel("USAGE");
				// graphics.disableAllMarkup();
				// formatterTemperature = new LineAndPointFormatter(Color.rgb(0,
				// 200, 0), Color.rgb(0, 0, 0), null);
				formatterRam = new LineAndPointFormatter(Color.rgb(0, 0, 128), Color.rgb(0, 0, 0), null);
				Log.i("CHECKING", "RANGED2");
				handlerGraphics = new Handler(new Handler.Callback() {
					@Override
					public boolean handleMessage(Message msg) {
						double[] msgs = (double[]) msg.obj;
						addMedicaoRam(msgs[1]);
						plot();
						return true;
					}
				});
			}
		});

	}

	public void addMedicaoTemperature(final int n) {

		medicoesTemperature.add(n);
		if (medicoesTemperature.size() > 20) {
			medicoesTemperature.remove(1);
		}

	}

	public void addMedicaoRam(final double n) {

		medicoesRam.add(n);
		if (medicoesRam.size() > 21) {
			medicoesRam.remove(1);
		}

	}

	public void plot() {
		// Number[] temperature = new Number[medicoesTemperature.size()];
		// for (int i = 0; i < temperature.length; i++) {
		// temperature[i] = medicoesTemperature.get(i);
		// }
		final Number[] ram = new Number[medicoesRam.size()];
		for (int i = 0; i < ram.length; i++) {
			ram[i] = medicoesRam.get(i);
		}
		father.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				graphics.clear();
				// graphics.addSeries(new
				// SimpleXYSeries(Arrays.asList(temperature),
				// SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Temperature C�"),
				// formatterTemperature);
				graphics.addSeries(new SimpleXYSeries(Arrays.asList(ram), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Ram usage"), formatterRam);
				graphics.invalidate();
			}
		});
	}
}
