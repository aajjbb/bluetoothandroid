package com.unitau.mrs;

import org.json.JSONException;
import org.json.JSONObject;

import com.unitau.Models.ModelCpu;
import com.unitau.Models.ModelHdd;
import com.unitau.Models.ModelRam;
import com.unitau.Models.ModelSystem;
import com.unitau.Models.ModelSystemInfo;
import com.unitau.Models.Parceable;

public class JSONParcer implements Parceable<ModelSystemInfo> {

	private final String DATA = "data";

	private final String SYSTEM = "system";
	private final String SO = "so";
	private final String USER = "user";
	private final String KERNEL_VERSION = "kernel_version";
	private final String ARCHITECTURE = "architecture";

	private final String HDD = "hdd";
	private final String FREE = "free";
	private final String TOTAL = "total";
	private final String USED = "used";
	private final String RAM = "ram";

	private final String CPU = "cpu";
	private final String CORES = "cores";
	private final String CORE_FREQUENCY = "core_frequency";
	private final String TEMPERATURE = "temperature";
	private final String MODEL_NAME = "model_name";
	private final String CACHE_SIZE = "cache_size";
	private final String IP = "ip";
	private final String BCC_CHECKSUM = "bcc_checksum";

	@Override
	public ModelSystemInfo parce(String json) {
		try {

			JSONObject parsed = new JSONObject(json);
			JSONObject dataArray = new JSONObject(parsed.getString(DATA));

			JSONObject systemArray = new JSONObject(dataArray.getString(SYSTEM));
			ModelSystem model_system = new ModelSystem(systemArray.getString(SO), systemArray.getString(USER), systemArray.getString(KERNEL_VERSION), systemArray.getString(ARCHITECTURE), systemArray.getString(IP));

			JSONObject hddArray = new JSONObject(dataArray.getString(HDD));
			ModelHdd model_hdd = new ModelHdd(hddArray.getDouble(FREE), hddArray.getDouble(TOTAL), hddArray.getDouble(USED));

			JSONObject ramArray = new JSONObject(dataArray.getString(RAM));
			ModelRam model_ram = new ModelRam(ramArray.getDouble(FREE), ramArray.getDouble(TOTAL), ramArray.getDouble(USED));

			JSONObject cpuArray = new JSONObject(dataArray.getString(CPU));
			ModelCpu model_cpu = new ModelCpu(cpuArray.getInt(CORES), cpuArray.getDouble(CORE_FREQUENCY), cpuArray.getDouble(TEMPERATURE), cpuArray.getString(MODEL_NAME), cpuArray.getInt(CACHE_SIZE));
			ModelSystemInfo sys_info = new ModelSystemInfo(model_system, model_hdd, model_ram, model_cpu);
			
			sys_info.setBCC(dataArray.getInt(BCC_CHECKSUM));
			return sys_info;
		} catch (JSONException e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

}
